const router = require('express').Router();
const Category = require('../models/category');

const checkJWT = require('../middlewares/check-jwt');
const Product = require('../models/product');

router.route('/category')
    .get((req, res, next) => {
            Category.find({}, (err, categories) => {
              res.json({
                success: true,
                message: "Success",
                categories: categories
              })
            })
        }
    )
    .post((req, res, next) => {
            Category.findOne({ name: req.body.category }, (err, category) => {
              if (category) {
                res.json("already exist")
              } else {
                  
                let category = new Category();
        
                category.name = req.body.category;
                category.save();
                res.json({
                  success: true,
                  message: "Successful"
                });
              }
            });
        }
    )

router.route('/products')
    .get(checkJWT, (req, res, next) => {
        Product.find({})
            .populate('category')
            .exec((err, products) => {
                if(products) {
                    res.json({
                        success: true,
                        message: 'Products',
                        products: products
                    });
                }
            })
    })
    .post(checkJWT, (req,res,next) => {
        let product = new Product();
        product.id = req.body.productId;
        product.category = req.body.categoryId;
        product.name = req.body.productname;
        product.description = req.body.description;
        product.unit = req.body.unit;
        product.save();
        res.json({
            success: true,
            message: 'Successfully Added'
        })
    })

module.exports = router;